#	card.py - part of libreEd
#		Matt Thompson 2007
#
#	libreEd is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation; either version 2 of the License, or
#	(at your option) any later version.
#
#	libreEd is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with Foobar; if not, write to the Free Software
#	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import sys, string

import md5

from xml.sax import ContentHandler, make_parser
from xml.sax.handler import feature_namespaces

import xml.dom.minidom as minidom

#import util

class Card:
	def clear(self):
		self.name = ""
		self.type = ""
		self.spirit_type = ""
		self.text = ""
		self.offense = ""
		self.health = ""
		self.cost = ""
		self.spirit = ""
		self.art = ""
		self.artist = ""
		self.path = ""
		self.creator = ""
		self.md5 = ""

	def set_from_dict(self,dict):
		self.name = dict["name"]
		self.type = dict["type"]
		self.text = dict["text"]
		self.offense = dict["offense"]
		self.health = dict["health"]
		self.cost = dict["cost"]
		self.spirit = dict["spirit"]
		self.art = dict["art"]
		self.creator = dict["creator"]

	def calculate_md5(self):
		hashstring = ""
		for key in dir(self):
			if not key.startswith('__'):
				value = getattr(self,key)
				if not callable(value):
					if value is not None:
						hashstring += value
		return md5.new(hashstring).hexdigest()

	def to_xml(self):
		self.md5 = self.calculate_md5() #Make sure md5 is up to date in case something changed after it was created
		impl = minidom.getDOMImplementation()

		newdoc = impl.createDocument(None, "card", None)
		top_element = newdoc.documentElement
		for key in dir(self):
			if not key.startswith('__'):
				value = getattr(self,key)
				if not callable(value):
					if value is not None:
						new_tag = newdoc.createElement(key)
						text = newdoc.createTextNode(value)
						new_tag.appendChild(text)
						top_element.appendChild(new_tag)
		return newdoc.toxml()

	def __init__(self, creator=None, name=None, spirit_type=None, type=None, text=None, offense=None, health=None, cost=None, spirit=None, art=None, artist=None):
		self.clear()
		self.creator = creator
		self.name = name
		self.type = type
		self.spirit_type = spirit_type
		self.text = text
		self.offense = offense
		self.health = health
		self.cost = cost
		self.spirit = spirit
		self.art = art
		self.artist = artist
		self.md5 = self.calculate_md5()

	def __str__(self):
		return self.to_xml()

	def __repr__(self):
		return "<Card: %s>" % self.name

def normalize_whitespace(text):
	return ' '.join(text.split())

class CardReader(ContentHandler):
	def __init__(self):
		self.card = Card()		
		self.elements = {
			"name":None,
			"type":None,
			"text":None,
			"offense":None,
			"health":None,
			"cost":None,
			"spirit":None,
			"art":None
		}
		self.current_element = None

	def startElement(self, name, attrs):
		if name == "card":
			self.card.clear()
		elif name in self.elements:
			self.current_element = ""
		else:
			print "!!!XML Parse Alert:\n\tElement <"+name+"> is not used."
			self.current_element = None
			
	def characters(self, ch):
		if self.current_element is not None:
			self.current_element = self.current_element + ch
		
	def endElement(self, name):
		if name == "card":
			# done reading self card, set up the card class
			self.card.set_from_dict(self.elements)
		elif name == "text": # only strip before and after
			self.elements[name] = self.current_element.strip().rstrip()
		elif self.current_element is not None:
			self.elements[name] = normalize_whitespace(self.current_element)
		self.current_element = None
		
	def error(self, exception):
		util.Notify(exception,"XML Error!")

def make_tag(tag,content):
	return "\t<"+tag+">\n\t\t"+content+"\n\t</"+tag+">\n"

class CardFactory:
	def __init__(self):
		self.cardreader = CardReader()
		self.parser = make_parser()
		self.parser.setFeature(feature_namespaces, 0)
		self.parser.setContentHandler(self.cardreader)
	
	def get_card(self):
		return self.cardreader.card
	
	def read(self,file):
		try:
			self.parser.parse(file)
			self.cardreader.card.path = file
			return True
		except:
			util.Notify("Could not read data from file '"+file+"'","Error!")
			return False
		
	def write(self,file):
		#try:
			card = self.cardreader.card
			f = open(file, 'w')
			f.write("<?xml version=\"1.0\"?>\n")
			f.write("<card>\n")
			f.write(make_tag("name",card.name))
			f.write(make_tag("type",card.type))
			f.write(make_tag("text",card.text))
			f.write(make_tag("offense",card.offense))
			f.write(make_tag("health",card.health))
			f.write(make_tag("cost",card.cost))
			f.write(make_tag("spirit",card.spirit))
			f.write(make_tag("art",card.art))
			f.write("</card>\n")
			f.close()
			return True
		#except:
			self.is_write = False
			util.Notify("Problem writing to file '"+file+"'","Error!")
			return False
