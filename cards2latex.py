import os

filelist = {"Earth":[], "Air":[], "Water":[], "Fire":[], "Metal":[], "Light":[], "Shadow":[], "Any":[], "Temples":[]}

def add_files(arg, directory, files):
	for f in files:
		if f[-3:] == "png":
			filelist[directory[8:]].append(directory+"/"+f)

os.path.walk("./cards/", add_files, None)

template = r'''
\documentclass[letter]{article}

\renewcommand{\baselinestretch}{0.01}

\usepackage[left=1.275cm,top=0.5cm,right=0.5cm,bottom=0.5cm,nohead,nofoot]{geometry}

\usepackage{graphicx}

%These three lines may not be needed anymore.
\DeclareGraphicsExtensions{.svg.png,..png}
\DeclareGraphicsRule{.svg.png}{png}{*}{}
\DeclareGraphicsRule{..svg.png}{png}{*}{}

\DeclareGraphicsRule{..png}{png}{*}{}

\begin{document}
'''

print template

for cardtype in filelist:
	counter = 0
	for image in filelist[cardtype]:
		counter += 1
		print (r'\vspace{-0.2mm}\noindent\includegraphics[width=2.5in]{%s}%%' % image)
		if (counter % 3) == 0:
			print r"\newline"
		if (counter % 9) == 0:
			#print r'\newpage'
			counter = 0
	print r'\newpage'
	
footer = "\end{document}"
print footer
