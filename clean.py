'''A script to take the dirty spoiler files and output clean XML.'''

import re
import os
import sys
import threading
from card import Card

STDOUT = True #Set to False to disable output to stdout

contents = sys.stdin.read()

card_list = []

rawstr = r"""Creator:\s*(?P<Creator>.*?)$
Name:\s*(?P<Name>.*?)\s?\((?P<SpiritType>.*?)\)$
Type:\s*(?P<Type>.*?)$
Text:\s*(?P<Text>.*?)
Offense:\s*(?P<Offense>.*?)$
Health:\s*(?P<Health>.*?)$
Cost:\s*(?P<Cost>.*?)$
Faith:\s*(?P<Faith>.*?)$
"""

compile_obj = re.compile(rawstr, re.MULTILINE | re.DOTALL)
match_obj = compile_obj.search(contents)

all_cards = compile_obj.findall(contents)


if not os.path.isdir("./cards/"):
	os.mkdir("./cards/")

types = ["Earth", "Air", "Water", "Fire", "Shadow", "Light", "Metal", "Any"]
optypes = ["Air", "Earth", "Fire", "Water", "Light", "Shadow", "Metal", "Any"]

for type in types:
	if not os.path.isdir("./cards/"+type):
		os.mkdir("./cards/"+type)

def writeCard(creator, name, spirit_type, type, text, offense, health, cost, spirit):
	newcard = Card(creator=creator, name=name, type=type, spirit_type=spirit_type, text=text, offense=offense, health=health, cost=cost, spirit=spirit)	
	card_list.append(newcard)
	if STDOUT:
		print str(newcard)
		return
	f = open("./cards/"+spirit_type+"/"+name.replace(" ", "_")+".xml", "w")
	f.write(str(newcard))
	f.close()

for creator, name, spirit_type, type, text, offense, health, cost, spirit in all_cards:
	if "~type~" in name.lower():
		for stype in types:
			if stype == "Any":
				continue
			op = optypes[types.index(stype)]
			writeCard(creator, name.replace("~type~", stype), stype, type, text.replace("~type~", stype).replace("~op~", op), offense, health, cost, spirit)
	else:
		writeCard(creator, name, spirit_type, type, text, offense, health, cost, spirit)


#~ print card_list
