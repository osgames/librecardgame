import sys
import os

import sys

#Supports giving the file as an argument or as stdin
if len(sys.argv) >= 2:
	with open(sys.argv[1]) as f:
		decklist = f.read()
else:
	decklist = sys.stdin.read()
	
decklist = decklist.strip()
decklist = decklist.split("\n")

#A list of tuples, index 0 is card name, index 1 is number of that card
deck = []

for card in decklist:
	if card == "" or card[0] == "#" or card[0:1] == "//" or card[0] == ";" or card[0] == "\n":
		if card[1:8].lower() == "newpage":
			deck.append(("newpage", 0))
		continue
	temp = card.split(",")
	deck.append((','.join(temp[0:-1]).strip(), int(temp[-1].strip())))

template = r'''
\documentclass[letter]{article}

\renewcommand{\baselinestretch}{0.01}

\usepackage[left=1.275cm,top=0.5cm,right=0.5cm,bottom=0.5cm,nohead,nofoot]{geometry}

\usepackage{graphicx}

\DeclareGraphicsExtensions{.svg.png}
\DeclareGraphicsRule{.svg.png}{png}{*}{}
\DeclareGraphicsRule{..svg.png}{png}{*}{}

\begin{document}
'''

print template

def find_card(arg, directory, files):
	for f in files:
		if f[-3:] == "png":
			filelist[directory[8:]].append(directory+"/"+f)

counter = 0
for card in deck:
	included = False
	if card[0] == "newpage":
		counter = 0
		print r'\newpage'
		continue
	for filename in os.walk("./cards/"):
		if card[0].replace(" ", "_")+".png" in filename[2]:
			included = True
			for i in range(int(card[1])):
				counter += 1
				image = ("%s/%s.png" % (filename[0], card[0].replace(" ", "_")))
				print (r'\vspace{-0.2mm}\noindent\includegraphics[width=2.5in]{%s}%%' % image)
				if (counter % 3) == 0:
					print r"\newline"
				if (counter % 9) == 0:
					print r'\newpage'
					counter = 0
	if included is False:
		#~ raise Exception, "Card '%s' was not found."%card[0]
		with open("missing_cards.log", "a") as f:
			f.write("Card '%s' was not found.\n\n"%card[0])
		continue
footer = "\end{document}"
print footer


