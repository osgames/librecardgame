import re
import sys

filename = sys.argv[1]

import xml.dom.minidom as md
from xml.dom.minidom import parse, parseString
import xml

import cgi

from generate_svg import getText

#~ xmlfile = md.parse(open(filename)).toprettyxml()

def pretty_print_file(filename):
	return '\n'.join([line for line in md.parse(filename).toprettyxml(indent=' '*2).split('\n') if line.strip()])

def pretty_print(xmldata):
	return '\n'.join([line for line in md.parseString(xmldata).toprettyxml(indent=' '*2).split('\n') if line.strip()])
	

#load the xml
#sort the tags as we need to
#write to a temporary xml file
#continue as normal, only give pretty_print the temp filename
def generate_card_dict(xmlfile, stdin=False):
	'''Gets card dict for first <card> element in the given xmlfile.  Returns a dict of card info.'''
	retval = []
	dom = parse(xmlfile)
	#output converted svg file
	for node in dom.getElementsByTagName("card"):
	
		#Fill the card dict automatically from available data in the XML
		card = dict()
		for n in node.childNodes:
			name = str(n.nodeName)
			if name[0] != "#":
				value = getText(node.getElementsByTagName(name)[0].childNodes).strip()
				if value == "NUL":
					value = "&#8212;"
				card.update({name:value})
				if "?" in value:
					try:
						if not options.stdout:
							print ("WARNING: %s has old unicode characters in %s." % (card["name"], str(name)))
					except(NameError):
						pass
	return card
	
	
d = generate_card_dict(filename)

tempxml = '<?xml version="1.0" ?><card>'
sortorder = ["name", "type", "spirit_type", "text", "offense", "health", "cost", "spirit", "creator", "arturi"]
for n in sortorder:
	try:
		tempxml += "<%s>%s</%s>" % (n, cgi.escape(d[n]), n)
	except(KeyError):
		tempxml += "<%s></%s>" % (n, n)
		
for k in d.keys():
	if k not in sortorder:
		try:
			tempxml += "<%s>%s</%s>" % (k, cgi.escape(d[k]), k)
		except(KeyError), exc:
			continue
tempxml += "</card>"

try:
	xmlfile = pretty_print(tempxml)
except(xml.parsers.expat.ExpatError), exc:
	print exc, tempxml
	raise exc

p = re.compile('  <md5>.*</md5>\n', re.DOTALL)
xmlfile = p.sub('', xmlfile)

p = re.compile('<path/>', re.DOTALL)
xmlfile = p.sub('', xmlfile)

xmlversion = '<?xml version="1.0" ?>'
xmlfile = xmlfile.replace(xmlversion, '\n<?xml-stylesheet type="text/css" href="card.css"?>\n')

dokufile = u""

#write it all into a file of the same name as the source, with the extension being txt. Overwrite without asking.

#write "<html>"
dokufile += "<html>\n"

#write '<?xml-stylesheet type="text/css" href="/card.css"?>'
#~ dokufile += '<?xml-stylesheet type="text/css" href="/card.css"?>\n'

#write a blank line
dokufile += "\n"
#write a comment: "CARD DATA BEGIN"
dokufile += "<!-- CARD DATA BEGIN -->\n"
#write a blank line
dokufile += "\n"

#write the contents of the xml file, sans the md5 line
dokufile += xmlfile
dokufile += "\n"

#write a blank line
dokufile += "\n"

#write a comment: "CARD DATA END"
dokufile += "<!-- CARD DATA END -->\n"

#write a blank line
dokufile += "\n"

#write "</html>"
dokufile += "</html>\n"

dokufile += "\n{{tag>"

#If the xml has no art tag, or an empty art tag, write a "noart" tag
#{{tag>noart}}
#If the xml has art, write a "needsartreview" tag
#{{tag>needsartreview}}
if d.has_key("arturi"):
	if d['arturi'] != "":
		dokufile += "needsartreview "
	else:
		dokufile += "noart "
else:
	dokufile += "noart "

types= ["Air", "Any", "Earth", "Water", "Fire", "Metal", "Light", "Shadow"]
#If the card name has its parent directory in its name, write a "coreset" tag
core = False
for t in types:
	if t in d['name']:
		dokufile += "coreset "
		core = True
		break
	#otherwise, write an "alphaset" tag (disable these two if the xml cards arent from these sets.
if not core:
	dokufile += "alphaset "

dokufile += "}}\n"

print dokufile.encode("utf-8")
	
