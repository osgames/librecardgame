'''
A script to:
download an image or use an existing one
use generate_svg.py to generate an svg image using the image provided as the background
change parameters of that image (pos, rot, scale) and regenerate to show preview
save out image parameters to the card's XML.
'''

from xml.dom.minidom import parse, parseString
import tempfile
import os, sys
import subprocess

import pygtk
pygtk.require('2.0')
import gtk
import gtk.glade

import generate_svg
from google_image_search import GoogleImageSearch

class Base:
	def __init__(self):
		self.gladefile = "card_image_layouter.glade"
		self.wTree = gtk.glade.XML(self.gladefile)
		
		self.svg = open('back.svg').read() #Default SVG
		
		self.lastcard = self.wTree.get_widget("card").get_text()
		self.load_new_card(self.wTree.get_widget("card").get_text())
		
		self.load_prefs()
					
		dic = { "on_need_update" : self.update,
			"on_MainWindow_destroy" : self.delete_event,
			"on_save" : self.write_xml,
			"on_preview": self.do_preview,
			"on_searchbutton_activate": self.get_images,
			"on_filechooser_selection_changed": self.set_background}
		self.wTree.signal_autoconnect(dic)
		
		chooser = self.wTree.get_widget("filechooser")
		chooser.set_current_folder("./tmp/")
		self.filter = gtk.FileFilter()
		self.filter.set_name("Images")
		for x in xrange(1, 21):
			self.filter.add_pattern(str(x))
		chooser.set_filter(self.filter)
		chooser.set_preview_widget(self.wTree.get_widget("imagepreview"))
		#chooser.add_filter(filter)

	def delete_event(self, widget=None, event=None, data=None):
		self.save_prefs()
		gtk.main_quit()
		return False
		
	def main(self):
		self.update()
		gtk.main()
		
	def set_background(self, *args):
		filename = self.wTree.get_widget("filechooser").get_filename()
		print filename
		filechooser = self.wTree.get_widget("filechooser")
		prevfilename = filechooser.get_preview_filename()
		print prevfilename
		imagepreview = self.wTree.get_widget("imagepreview")
		
		filechooser.set_preview_widget_active()
		pixbuf = gtk.pixbuf_new_from_file(prevfilename)
		imagepreview.set_pixbuf(pixbuf)
		
		if filename is not None:
			self.wTree.get_widget("arturi").set_text(str(filename))
		
	def get_images(self, *args):
		query = self.wTree.get_widget("searchbox").get_text()
		gis = GoogleImageSearch()
		gis.defaults()
		#gis.query_and_download(query)

		chooser = self.wTree.get_widget("filechooser")
		chooser.set_current_folder("./tmp/")
		chooser.set_filter(self.filter)
		
	def save_prefs(self):
		#Write card settings prefs.py
		try:
			f = open("./prefs.py", "w")
			lastcard = self.wTree.get_widget("card").get_text()
			f.write("lastcard = '%s'\n" % (lastcard,))
			f.write("lastpage = %i\n" % (self.wTree.get_widget("notebook").get_current_page(),))
			f.write("hq_preview = %s\n" % (self.wTree.get_widget("hq_preview").get_active()))
			f.close()
			print "saved lastcard as", lastcard
		except (), e:
			print e
			
	def load_prefs(self):
		try:
			import prefs
			oldcard = prefs.lastcard
			print "loaded lastcard as", prefs.lastcard
			self.load_new_card(oldcard)
			self.wTree.get_widget("notebook").set_current_page(prefs.lastpage)
			self.wTree.get_widget("hq_preview").set_active(prefs.hq_preview)
		except (IOError, ImportError, AttributeError), e:
			print e, ", but dont worry about it."
		
	def update_image(self, contents=None, svg=True):
		if svg is not True:
			loader = gtk.gdk.PixbufLoader("png")
		else:
			loader = gtk.gdk.PixbufLoader("svg")
		if contents is None:
			loader.write(self.svg)
		else:
			loader.write(contents)
		loader.close()
		self.wTree.get_widget("image").set_from_pixbuf(loader.get_pixbuf())
		
	def update(self, *args): #Some signals give us extra arguments
		print "updating"
		
		for x in self.wTree.get_widget_prefix(""):
			if x.name in self.card_dict:
				if 'get_text' in dir(x):
					self.card_dict.update({x.name:x.get_text()})
				elif 'get_value' in dir(x):
					self.card_dict.update({x.name:x.get_value()})
				elif 'get_buffer' in dir(x):
					text = x.get_buffer().get_text(x.get_buffer().get_start_iter(), x.get_buffer().get_end_iter())
					self.card_dict.update({x.name:text})
				elif 'get_active' in dir(x):
					model = x.get_model()
					active = x.get_active()
					if active < 0:
						text = ""
					else:
						text = model[active][0]
					self.card_dict.update({x.name:text})
		
		if self.wTree.get_widget("card").get_text() != self.lastcard:
			try:
				self.lastcard = self.wTree.get_widget("card").get_text()
				counter = 0
				while counter < 10:
					self.load_new_card(self.wTree.get_widget("card").get_text())
					counter += 1
				print "loading new card"
			except (IOError):
				pass
				
		try:
			self.card_dict.update( {"arturi" : self.wTree.get_widget("arturi").get_text(), 
						"rotate" : self.wTree.get_widget("rotate").get_value(),
						"scalex" : self.wTree.get_widget("scalex").get_value(),
						"scaley" : self.wTree.get_widget("scaley").get_value(),
						"translatex" : self.wTree.get_widget("translatex").get_value(),
						"translatey" : self.wTree.get_widget("translatey").get_value(),
						})
			if -0.001 < self.card_dict["scaley"] < 0.001:
				self.card_dict.update({"scaley":self.card_dict["scalex"]})
			self.svg = generate_svg.cardsvg.substitute(self.card_dict)
			self.update_image()
			if self.wTree.get_widget("hq_preview").get_active():
				self.do_preview()
		except (IOError), e:
			print e
			
		self.wTree.get_widget("searchbox").set_text(self.wTree.get_widget("name").get_text())

	def load_new_card(self, cardxml):
		self.wTree.get_widget("card").set_text(cardxml)
		self.card_dict = generate_svg.generate_card_dict(cardxml)
		if self.card_dict["scaley"] == self.card_dict["scalex"]:
			self.card_dict["scaley"] = 0
		string_image_settings = [u"arturi",]
		float_image_settings = [u"rotate", u"scalex", u"scaley", u"translatex", u"translatey"]
		image_settings = string_image_settings+float_image_settings+self.card_dict.keys()
		
		for x in self.wTree.get_widget_prefix(""):
			if x.name in self.card_dict:
				if 'set_text' in dir(x):
					x.set_text(str(self.card_dict[x.name]))
				elif 'set_value' in dir(x):
					x.set_value(self.card_dict[x.name])
				elif 'set_buffer' in dir(x):
					buf = x.get_buffer()
					buf.set_text(self.card_dict[x.name])
					x.set_buffer(buf)
				elif 'set_active' in dir(x):
					count = -1
					for m in x.get_model():
						count += 1
						if m[0] == str(self.card_dict[x.name]):
							x.set_active(count)

		for setting in image_settings:
			if setting in self.card_dict:
				if self.card_dict[setting] != "":
					try:
						self.wTree.get_widget(setting).set_value(float(self.card_dict[setting]))
#						print ("set value %s to %s" % (setting, float(self.card_dict[setting])))
					except (AttributeError):
						try:
							self.wTree.get_widget(setting).set_text(self.card_dict[setting])
#							print ("set text %s to %s" % (setting, self.card_dict[setting]))
						except (AttributeError), e:
							try:
								buf = self.wTree.get_widget(setting).get_buffer()
								buf.set_text(self.card_dict[setting])
								self.wTree.get_widget(setting).set_buffer(buf)
							except (AttributeError), e:
								if self.wTree.get_widget(setting):
									#print e
									pass

	def write_xml(self, *args):
		'''Writes the card's data back out to XML, overwriting the original.'''
		image_settings = ["arturi", "rotate", "scalex", "scaley", "translatex", "translatey"]
		font_settings = ["text_size", "name_size", "typetext_size"]
		card_settings = ["cost", "creator", "health", "md5", "name", "offense", "spirit", "spirit_type", "text", "type"]
		settings = image_settings+font_settings+card_settings
		filename = self.wTree.get_widget("card").get_text()
		dom = parse(filename)
		print self.card_dict
		for key in settings:
			#Remove the node we're getting ready to add, otherwise a duplicate gets added.
			for node in dom.getElementsByTagName("card")[0].getElementsByTagName(key):
				dom.getElementsByTagName("card")[0].removeChild(node)
			newelement = dom.createElement(key)
			newelement.appendChild(dom.createTextNode(str(self.card_dict[key])))
			dom.getElementsByTagName("card")[0].appendChild(newelement)
		
		f = open(filename, "w")
		f.write(dom.toxml())
		f.close()
		print ("Saved %s" % filename)
	
	def do_preview(self, *args):
                #svgtemp = os.path.abspath("./gonnagetdeletedautomatically.svg")
                #pngtemp = os.path.abspath("./gonnagetdeletedautomatically.png")
                svgtemp = "./gonnagetdeletedautomatically.svg"
	        pngtemp = "./gonnagetdeletedautomatically.png"
		s = open(svgtemp, "w")
		s.write(self.svg)
		s.close()

                print
                print svgtemp
                print pngtemp
                print

                if sys.platform == "win32":
                        inkscape_path = r'N:\Program Files\Inkscape\inkscape.exe'
                else:
                        inkscape_path = 'inkscape'
		
		command = ('%s -C -d 90 --export-png=%s -z -f %s' % (inkscape_path, pngtemp, svgtemp))
		print(command)
		try:
	                #print subprocess.call(command)
	                print os.system(command) #Slow but it works :<
                except(OSError), e:
                	print "inkscape fails"
                	sys.exit()
                	#print e
                	pass


                try:
	                print os.path.isfile(pngtemp)
			f = open(pngtemp, "rb")
		except(OSError), e:
			print "damn."
		self.update_image(contents=f.read(), svg=False)
		f.close()
		
		os.unlink(svgtemp)
		os.unlink(pngtemp)
	
if __name__ == "__main__":
	base = Base()
	base.main()
	
