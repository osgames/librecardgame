import os

filelist = []

def add_files(arg, directory, files):
	for f in files:
		if f[-3:] == "png":
			filelist.append(directory+"/"+f)

os.path.walk("./cards/", add_files, None)

template = '''<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
	<title>Some Cards</title>
	<style type="text/css">
	<!--
	@page {
	  margin: 1px;
	}

	img {
	  float: left;
	  width:2.5in;
	}

	br {
	  clear: both;
	}

	div {
	  page-break-after:always;
	}
	-->
	</style>

    </head>
    <body>

'''

print template

counter = 0
for image in filelist:
	counter += 1
	print ('<img src="%s" alt="a card"/>' % image)
	if (counter % 3) == 0:
		print "<br />"
	if (counter % 9) == 0:
		print '<div />'
		counter = 0

footer = "</body></html>"
print footer
