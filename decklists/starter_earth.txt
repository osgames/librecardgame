#A basic starter deck focusing on Earth
#Total: 60

#Some traditional base cards: 12
Earth Paladin, 4
High Priest of Earth, 4
Earth Conversion, 4

#And some stompy: 4
Basilisk, 2
Edmontia, 2

#And some crowd control: 4
Landslide, 4

#Now for some Dwarfs to make the deck really rumble!: 18
Tyre, 2
Dwarven Worker, 4
Dwarf Blacksmith, 4
Warhammer, 4
Dwarven Battle Axe, 4

#And finally, a little pump action: 4
Ashes of a Loved One, 4

#newpage

#First, some Temples.  We might need more for Earth if its Spirit heavy: 18
Earth Temple, 18
