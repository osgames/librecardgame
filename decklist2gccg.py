import sys
import os
import shutil
import pipes
from xml.dom.minidom import parse, parseString

HEIGHT = 400

#Supports giving the file as an argument or as stdin
if len(sys.argv) >= 2:
	with open(sys.argv[1]) as f:
		decklist = f.read()
else:
	decklist = sys.stdin.read()

decklist = decklist.strip()
decklist = decklist.split("\n")

#A list of tuples, index 0 is card name, index 1 is number of that card
deck = []

for card in decklist:
	if card == "" or card[0] == "#" or card[0:1] == "//" or card[0] == ";" or card[0] == "\n":
		if card == "#newpage" or card == "#newline" or card == "\n" or card == "":
			continue
		temp = card.split(",")
		#~ print temp
		setname = temp[0][1:]
		setdir = setname
		setabbrev = temp[1]
		continue
	temp = card.split(",")
	temp = [','.join(temp[:-1]), temp[-1]]
	deck.append((temp[0].strip(), int(temp[1].strip())))

template = r'''<?xml version='1.0' encoding='latin1'?>
<!DOCTYPE ccg-setinfo SYSTEM "../gccg-set.dtd">
<ccg-setinfo name="%(setname)s" dir="%(setdir)s" abbrev="%(setabbrev)s" game="Libre: The Open Card Game">
 <cards>
'''
template = template % ({"setname":setname, "setdir":setdir, "setabbrev":setabbrev})
print template

def find_card(arg, directory, files):
	for f in files:
		if f[-3:] == "xml":
			filelist[directory[8:]].append(directory+"/"+f)

def getText(nodelist):
	rc = ""
	for node in nodelist:
		if node.nodeType == node.TEXT_NODE:
 			rc = rc + node.data
	return rc

for card in deck:
	included = False
	for filename in os.walk("./cards/"):
		if card[0].replace(" ", "_")+".xml" in filename[2]:
			included = True
			image = ("%s/%s.xml" % (filename[0], card[0].replace(" ", "_")))
			import generate_svg as gs
			cardinfo = gs.generate_card_dict(image)
			for thing in cardinfo:
				cardinfo.update({thing:unicode(cardinfo[thing]).replace(u"\u2014", "NUL")})
				cardinfo.update({thing:unicode(cardinfo[thing]).replace("&#8212;", "NUL")})
				
			cardinfo.update({"art":cardinfo["name"].replace(" ", "_")+".jpg"})
			cardinfo.update({"text":cardinfo["text"].replace('"', "").replace("'", "")})
			
			try:
				os.mkdir(setdir)
			except (OSError):
				pass
			
			sourcefile = "./cards/%s/%s" % (cardinfo['spirit_type'], cardinfo['art'].replace(".jpg", ".png").replace("'","\'"))
			destfile = "./%s/%s" % (setdir, cardinfo['art'].replace(".jpg", ".png"))
			shutil.copy(sourcefile, destfile)

			os.system("convert -resize x%(height)s ./%(setdir)s/%(original_image)s ./%(setdir)s/%(card_art)s" % {"original_image":pipes.quote(cardinfo['art'].replace(".jpg",".png")), "card_art":pipes.quote(cardinfo['art']), "setdir":setdir, "height":str(HEIGHT)})
			
			pointvalue = 0
			for key in ["offense", "health", "spirit", "cost"]:
				try:
					pointvalue += int(cardinfo[key])
				except(ValueError):
					continue
			pointvalue /= 4.0
			if pointvalue < 1.75:
				rarity = "C"
			elif pointvalue < 2:
				rarity = "U"
			elif pointvalue >= 2.5:
				rarity = "R"
			#if cardinfo['type'] == "Temple":
				#rarity = "T"
			cardinfo.update({'rarity':rarity})
			cardinfo.update({'version':sys.argv[2]})
			cardstring = '''  <card name="%(name)s" graphics="%(art)s" text="%(text)s">
   <attr key="type" value="%(type)s" />
   <attr key="spirit_type" value="%(spirit_type)s" />
   <attr key="offense" value="%(offense)s" />
   <attr key="health" value="%(health)s" />
   <attr key="spirit_cost" value="%(cost)s" />
   <attr key="faith" value="%(spirit)s" />
   <attr key="md5sig" value="%(md5)s" />
   <attr key="artist" value="%(artist)s" />
   <attr key="rarity" value="%(rarity)s" />
   <attr key="version" value="%(version)s" />
  </card>'''
			print (cardstring % (cardinfo))
	if included is False:
		raise Exception, "Card '%s' was not found."%card[0]
footer = r""" </cards>
</ccg-setinfo>"""
print footer


