#!/usr/bin/fish

#This is the version that your built files are tagged as
set LIBREVERSION "2.7"

#This is the unix timestamp of the last release.
#The updater will pull card data from the wiki for all card pages newer than this date.
#Making it less than the current release only makes you have to download files you already have.
set LASTRELEASE 1274752074

set CCBY3 "This work is licensed under the Creative Commons Attribution 3.0 United States License. To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/us/ or send a letter to Creative Commons, 171 Second Street, Suite 300, San Francisco, California, 94105, USA."
set CCBYSA3 "This work is licensed under the Creative Commons Attribution-Share Alike 3.0 United States License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/us/ or send a letter to Creative Commons, 171 Second Street, Suite 300, San Francisco, California, 94105, USA."

#Update our card xml from the wiki:
echo "Updating..."
python update_cardxml.py $LASTRELEASE

#Clean up our old svgs and any backup files from editors, keep pngs around to use to determine what to rerender
#rm cardlists/*~ cards/*/*svg* cards/*/*.png;
rm cardlists/*~ cards/*/*svg*;

#Generate our SVGs
#python generate_svg.py -m True;  #Uncomment me to generate monochrome images
echo "Generating SVGs"
python generate_svg.py; 

#Render our SVGs and optimize them as we go
#TODO: check if our files have changed so we dont do more work than we have to
for i in cards/*/*.svg;
	echo;
	echo "Converting: " $i; 
	echo;
	if test -e (basename $i .svg).png;
		set FILEMODTIME (stat --format=%Y (basename $i .svg).png);
	else;
		set FILEMODTIME 0;
	end;
		
	if test $FILEMODTIME -lt $LASTRELEASE;
		inkscape -C -d 300 -e (dirname $i)/(basename $i .svg).png -z -f $i; 
		optipng -q (dirname $i)/(basename $i .svg).png&; #Send it to the background so we can keep converting svgs (works great on >1 core systems)
	else;
		echo "Skipping $i"
	end;
end;

for i in (jobs -p); fg $i; end

#TODO: Include XMP metadata for licensing in the PDF: http://www.ctan.org/tex-archive/macros/latex/contrib/xmpincl/
#Generate latex files for printing at home.
python cards2latex.py > alpha.tex;

#All the files we have in our cards directory
echo "PDFing Alpha cards."
pdflatex alpha.tex; 
mv alpha.pdf cards-alpha-$LIBREVERSION.pdf

#INFO: If the next two fail, its because you're missing a card that is in the given decklist.
#Type \end, hit enter, type \end, and hit enter again to continue.
#Just Temples
python decklist2latex.py decklists/temples.txt > temples.tex
echo "PDFing Temple cards."
pdflatex temples.tex; #All the temples, each to a page.
mv temples.pdf cards-temples-$LIBREVERSION.pdf

#Just Core set cards
echo "PDFing Core cards."
python decklist2latex.py decklists/core.txt > core.tex
pdflatex core.tex; #All the core cards
mv core.pdf cards-core-$LIBREVERSION.pdf

#Clean up generated files
rm *.aux *.log alpha.tex temples.tex core.tex;

#Zip up our images and source files
echo $CCBY3 > LICENSE_CCBY3
echo $CCBYSA3 > LICENSE_CCBYSA3

echo "Zipping up..."

zip cards-xml-$LIBREVERSION.zip -r ./cards/ ./cards/ -i \*.xml
zip cards-xml-$LIBREVERSION.zip LICENSE_CCBY3

zip cards-svg-$LIBREVERSION.zip -r ./cards/ ./cards/ -i \*.svg 
zip cards-svg-$LIBREVERSION.zip LICENSE_CCBYSA3

zip cards-png-$LIBREVERSION.zip -r ./cards/ ./cards/ -i \*.png 
zip cards-png-$LIBREVERSION.zip LICENSE_CCBYSA3

rm LICENSE_CCBY3 LICENSE_CCBYSA3

rm -r ./build/
mkdir build

mv -f ./cards-alpha-$LIBREVERSION.pdf ./cards-temples-$LIBREVERSION.pdf ./cards-core-$LIBREVERSION.pdf ./cards-png-$LIBREVERSION.zip ./cards-xml-$LIBREVERSION.zip ./cards-svg-$LIBREVERSION.zip -t build/

#Now, we'll build the GCCG files.

#Copy the base files into the build directory:
cp -R ./gccg-libre/ ./build/
mv ./build/gccg-libre/ ./build/gccg-libre-$LIBREVERSION/

echo "Building GCCG package..."

#Now generate the xml:
#TODO: Generate the alpha decklist automatically by getting all cards in the alphaset tag.
python decklist2gccg.py decklists/alpha.txt $LIBREVERSION > ./build/gccg-libre-$LIBREVERSION/xml/Libre/alpha.xml
python decklist2gccg.py decklists/core.txt $LIBREVERSION > ./build/gccg-libre-$LIBREVERSION/xml/Libre/core.xml

#Now copy the smaller images into the gccg builddir:
mv ./Core/*.jpg ./build/gccg-libre-$LIBREVERSION/graphics/Libre/Core/
mv ./Alpha/*.jpg ./build/gccg-libre-$LIBREVERSION/graphics/Libre/Alpha/

#Clean up some of the card images that stick around.
rm ./Core/*.png ./Alpha/*.png
rmdir ./Core/ ./Alpha/

#Add the back
convert -resize x400 back.png ./build/gccg-libre-$LIBREVERSION/graphics/Libre/back.jpg

#Now add the default gccg stuff.
cd ./build/gccg-libre-$LIBREVERSION/
wget http://gccg.sourceforge.net/modules/gccg-core-1.0.1.tgz
tar xzvf gccg-core-1.0.1.tgz
rm gccg-core-1.0.1.tgz
./gccg_package update
./gccg_package install client server darwin-i386 docs fonts fonts-windows linux-i386 windows32
cd ../../

#And finally zip it up
zip -r ./build/gccg-libre-$LIBREVERSION.zip ./build/gccg-libre-$LIBREVERSION
#Also gzip it for distribution, and gzip the graphics folder separately so it goes in the cards package.
tar --directory=./build/gccg-libre-$LIBREVERSION/ -cvzpf ./build/gccg-libre-cards-$LIBREVERSION.tgz ./graphics/
rm -r ./build/gccg-libre-$LIBREVERSION/graphics/ 
tar --directory=./build/gccg-libre-$LIBREVERSION/ -cvzpf ./build/gccg-libre-$LIBREVERSION.tgz ./
rm -r ./build/gccg-libre-$LIBREVERSION/

echo '<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE modules SYSTEM "modules.dtd">
<modules>
  <source url="http://gccg.sourceforge.net/modules/"/>
  <module name="libre" version="'$LIBREVERSION'"></module>
  <module name="libre-cards" version="'$LIBREVERSION'"></module>
</modules>' > ./build/available.xml;

#echo "Pushing to the File Release System. Remember to log in and change it to the recommended version."
rsync -v -e ssh build/*.pdf gryc_ueusp,librecardgame@frs.sourceforge.net:'/home/frs/project/l/li/librecardgame/Card\ PDFs/'&; 
rsync -v -e ssh build/cards*png*.zip gryc_ueusp,librecardgame@frs.sourceforge.net:'/home/frs/project/l/li/librecardgame/Card\ Images/'&;
rsync -v -e ssh build/cards*svg*.zip gryc_ueusp,librecardgame@frs.sourceforge.net:'/home/frs/project/l/li/librecardgame/Card\ SVG/'&;
rsync -v -e ssh build/cards*xml*.zip gryc_ueusp,librecardgame@frs.sourceforge.net:'/home/frs/project/l/li/librecardgame/Card\ XML/'&;
rsync -v -e ssh build/gccg-libre-*.zip gryc_ueusp,librecardgame@frs.sourceforge.net:'/home/frs/project/l/li/librecardgame/Play\ Online/'&;
rsync -v -e ssh build/gccg-libre-*.tgz gryc_ueusp,librecardgame@frs.sourceforge.net:'/home/groups/l/li/librecardgame/htdocs/gccg/'&;
rsync -v -e ssh build/available.xml gryc_ueusp,librecardgame@frs.sourceforge.net:'/home/groups/l/li/librecardgame/htdocs/gccg/'&;
rsync -rv -e ssh cards/ gryc_ueusp,librecardgame@frs.sourceforge.net:'/home/groups/l/li/librecardgame/htdocs/cards/'&;
