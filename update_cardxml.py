'''Pulls cards from the wiki and updates your local card xml files.'''

import os
import sys
import getopt
from urllib import urlencode

# CONFIG
WIKI='http://librecardgame.sourceforge.net/dokuwiki'

try:
    import xmlrpclib
except ImportError:
    print "This script needs the xmlrpclib module!"
    sys.exit(1)

if __name__ == '__main__':
    url = WIKI + "/lib/exe/xmlrpc.php"
    rpc = xmlrpclib.ServerProxy(url)

    pages = rpc.dokuwiki.getPagelist("cards", [])
    changed = {}
    for p in pages:
        if int(p['rev']) > int(sys.argv[1]):
            page = rpc.wiki.getPage(p['id'])
            changed.update({p['id']:page})
    print "%d cards changed since last release." % len(changed)
    
    for c in changed.keys():
        pagedata = changed[c]
        
        pagedata = pagedata.split("<!-- CARD DATA BEGIN -->")[1].split("<!-- CARD DATA END -->")[0]
        
        pagedata = '<?xml version="1.0" ?>'+pagedata
        
        name = pagedata.split("<name>")[1].split("</name")[0].strip().replace(" ", "_")
        color = pagedata.split("<spirit_type>")[1].split("</spirit_type")[0].strip()
        
        path = "./cards/%s/%s.xml" % (color, name)
        
        print name, color, len(pagedata), path, os.path.exists(path)
        
        with open(path, "w") as f:
			f.write(pagedata)
        
        #~ print pagedata
        
        
        
