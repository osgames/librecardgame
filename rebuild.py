import os

#Remove the "-m True" below to make it not print in monochrome.
#That is there to save ink when printing on a home printer.

os.system("rm cardlists/*~ cards/*/*svg*;")
os.system("python generate_svg.py -m True;")
for i in os.walk("./cards/"):
	if i[2][-3:] == "svg":
		print "converting", i
		if os.path.getmtime(i[0]+"/"+i[2]) > os.path.getmtime(i[0]+"/"+i[2]+".png")
		inkscape -C -d 300 -e $i.png -z -f $i &> /dev/null; 
end; 
python cards2latex.py > test.tex; 
pdflatex test.tex; 
pdflatex temples.tex; 
pdflatex core.tex; 
rm *.aux *.log;

