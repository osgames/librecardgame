#BeautifulSoup version 3.0.7a required. 3.1 will not work.

import urllib, urllib2
import re
import os
from BeautifulSoup import BeautifulSoup

DEFAULT_QUERY = "http://images.google.com/images?q=QueryGoesHere"

class GoogleImageSearch(object):
    def __init__(self, useragent="GIS v0.1", url=DEFAULT_QUERY):
        self.useragent = useragent
        self.values = {"User-Agent":self.useragent}
        self.url = url
        self.lastsoup = None
    def defaults(self):
        '''Sets the whole thing to a default state.
        I'm putting this here because most of the strings are way too long.'''
        
        self.useragent = "Mozilla/4.0 (compatible; MSIE 4.0; Windows NT)" 
        #Ancient UA so google doesnt try to give us the page via javascript
        self.url = ("http://images.google.com/images?imgsz=xga&imgtbs=rz"
                    "&as_st=y&as_rights=%28cc_publicdomain|cc_attribute|"
                    "cc_sharealike%29.-%28cc_noncommercial|cc_nonderived%29"
                    "&hl=en&safe=off&client=firefox-a&rls=com.ubuntu%3Aen-US"
                    "%3Aunofficial&um=1&sa=1&q=QueryGoesHere"
                    "&btnG=Search+images")
                    
    def query(self, query):
        tempurl = self.url.replace("QueryGoesHere", urllib.quote_plus(query))
        values = {"User-Agent":self.useragent}
        req = urllib2.Request(url=tempurl, headers=values)
        req.version = self.useragent
        response = urllib2.urlopen(req)
        page =  response.read()
        soup = BeautifulSoup(page)
        thumbs = [x['src'] for x in soup('img', src=re.compile('^http.*'))]
        links = [x['href'] for x in soup('a', href=re.compile('^/imgres?.*'))]
        fullimages = []
        imagerefs = []
        for link in links:
            link = link.replace('/imgres?imgurl=', '')
            urls = link.split('&')
            fullimages.append(urls[0])
            imagerefs.append(urls[1].replace("imgrefurl=",""))
        return zip(thumbs, fullimages, imagerefs)
    def clear(self, number, files=None):
        if files is None:
            files = ["tmp/"+str(number), 
                    "tmp/thumb_"+str(number), 
                    "tmp/"+str(number)+".html"]
                    
        for filename in files:
            try:
                os.remove(filename)
            except (OSError):
                continue
                
    def query_and_download(self, query):
        '''Downloads all 20 images, their thumbnails, and their homepage
        to a directory "tmp".  
        Returns None.'''
        counter = 0
        bowl = self.query(query)
        for thumb, link, origpage in bowl:
            print "Downloading", link.split("/")[-1]
            counter += 1
            with open("tmp/"+str(counter)+".html", "wb") as filehandle:
                try:
                    req = urllib2.Request(url=origpage, headers=self.values)
                    urlobj = urllib2.urlopen(req)
                    data = urlobj.read()
                    filehandle.write(data)
                except(urllib2.HTTPError):
                    #We dont want anything else if we cant get the page
                    self.clear(counter)
                    continue
            with open("tmp/"+str(counter), "wb") as filehandle:
                try:
                    req = urllib2.Request(url=link, headers=self.values)
                    urlobj = urllib2.urlopen(req)
                    data = urlobj.read()
                    filehandle.write(data)
                except(urllib2.HTTPError):
                    #If it fails, dont get the thumbnail, and clean up
                    self.clear(counter)
            with open("tmp/thumb_"+str(counter), "wb") as filehandle:
                try:
                    req = urllib2.Request(url=thumb, headers=self.values)
                    urlobj = urllib2.urlopen(req)
                    data = urlobj.read()
                    filehandle.write(data)
                except(urllib2.HTTPError):
                    #We dont care if we can get the thumbnail or not.
                    pass


def main():
    gis = GoogleImageSearch()
    gis.defaults()
    gis.query_and_download("porn")

if __name__ == "__main__":
    main()
	

