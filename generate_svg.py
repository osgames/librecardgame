'''A script to take the clean XML and apply it to the SVG template to generate SVG images.'''

from __future__ import division
import os, sys
import textwrap
from string import Template
from xml.dom.minidom import parse, parseString
from optparse import OptionParser
from PIL import Image

import hashlib

MONOCHROME = False
CARDSDIR = os.path.abspath("./cards/")
SYMBOLSDIR = os.path.abspath("./symbols/")

HASHSORTORDER = ["name", "type", "spirit_type", "text", "offense", "health", "cost", "spirit"]

#Open template.svg
f = open("template.svg", "r")
template = f.read()
f.close()

cardsvg = Template(template)

def getText(nodelist):
	rc = ""
	for node in nodelist:
		if node.nodeType == node.TEXT_NODE:
 			rc = rc + node.data
	return rc

def bezier(A, B, C, t):
    """A B and C being 2-tuples, t being a float between 0 and 1
    Author: Francesco Mastellone (effeemme)
    Website: http://www.pygame.org/project/686/"""
    x = (1 - t)**2 * A[0] + 2 * t * (1 - t) * B[0] + t**2 * C[0]
    y = (1 - t)**2 * A[1] + 2 * t * (1 - t) * B[1] + t**2 * C[1]
    return x, y

def get_font_size_accurate(text, boxwidth=186, boxheight=60, debug=""):
	done = False
	lines = [line.strip() for line in text.splitlines()]
	for line in lines:
		if line == "":
			lines.remove(line)
	fontsize = 10
	while not done:
		if len(lines)*fontsize > boxheight:
			#fontsize *= boxheight/(len(lines)*fontsize)
			fontsize -= 1
			#print "Woo, easy out! Text wont fit by default, even if the lines dont wrap. Decreasing fontsize."
			continue
		
		#Now, we'll split it up into hardwrapped lines.
		newlines = list()
		for line in lines:
			if len(line)*(fontsize/2) < boxwidth: #See if the line fits inside the box as is.
				newlines.extend([line])
			else:
				charsperline = int(boxwidth/(fontsize/2))
				line = textwrap.wrap(line, charsperline)
				newlines.extend(line)
		for newline in newlines:
			if newline == "":
				newlines.remove(newline)
				
		#And see if they fit.
		if len(newlines) > 0:
			#print debug, "numlines:",len(newlines), "fontsize:",fontsize, "mult:", boxheight/len(newlines)*fontsize,
			#print newlines
			if (((len(newlines)+2)*fontsize) > boxheight):
				#fontsize = fontsize*((boxheight/(len(newlines)*fontsize)))
				fontsize -= 1
				#print "Text wont fit, even with wrapped lines. Decreasing fontsize.", fontsize
				continue
			else:
				#print "Text fit, ending at:", fontsize
				done = True
		else:
			fontsize = 0
			done = True
			continue
	return fontsize
		

def get_font_size(text, numlines=4, boxwidth=185, boxheight=64):
	#Take into account the number of newlines and the minimum space required.
	#Like, four lines can have a maximum font size of 10, etc.
	#Perhaps average the length of lines and have that effect the weighting. Logrithmic averaging?
	C = [5, 62.5*numlines]
	B = [8, 50*numlines]
	A = [12, 25*numlines]
	t = len(text)/(C[1]*1.5)
	if t > 1: t = 1
	if t < 0: t = 0
	size = bezier(A, B, C, t)[0]
	if size > 12: size = 12
	if size < 6: size = 6
	return size

def get_spirit_type_color(st):
	if MONOCHROME: return "white"
	if st == "Earth": return "#5fd35f"
	elif st == "Air": return "#ffe680"
	elif st == "Water": return "#2a7fff"
	elif st == "Fire": return "#ff5555"
	elif st == "Metal": return "#c0c0c0"
	elif st == "Light": return "#ffffff"
	elif st == "Shadow": return "#575757"
	else: return "#ab9c86"

def generate_card_dict(xmlfile, stdin=False):
	'''Gets card dict for first <card> element in the given xmlfile.  Returns a dict of card info.'''
	retval = []
	dom = parse(xmlfile)
	#output converted svg file
	for node in dom.getElementsByTagName("card"):
	
		#Fill the card dict automatically from available data in the XML
		card = dict()
		for n in node.childNodes:
			name = str(n.nodeName)
			if name[0] != "#":
				value = getText(node.getElementsByTagName(name)[0].childNodes).strip()
				if value == "NUL":
					value = "&#8212;"
				card.update({name:value})
				if "?" in value:
					try:
						if not options.stdout:
							print ("WARNING: %s has old unicode characters in %s." % (card["name"], str(name)))
					except(NameError):
						pass
	
		
	
		#Do some cleanup and formatting things that really should be in the template.
		card["spirit_type_color"] = get_spirit_type_color(card["spirit_type"])
		card["text"] = card["text"].replace("~this~", card["name"])
		card["text"] = card["text"].replace("~This~", card["name"])
		card["text"] = card["text"].replace("~N~", "\n")
		card["text"] = card["text"].replace("~n~", "\n")
		card["text"] = card["text"].replace("&#8212;", "")
		

		
		card["spirit_type_image"] = os.path.abspath(SYMBOLSDIR+"/"+card["spirit_type"]+".png")
		if "arturi" not in card:
			#If arturi isnt defined, set it to the card's name as a png.
			card["arturi"] = card["name"]+".png"
			#Or just set it to blank...
			card["arturi"] = "../../mono_bg.png"
		elif card["arturi"] == "":
			card["arturi"] = "../../mono_bg.png"
			
		if MONOCHROME:
			card["arturi"] = "../../mono_bg.png"
		if "artist" not in card:
			#If artist isnt defined, stick our URI in there.
			card["artist"] = "librecardgame.sourceforge.net"
		#If the arturi isnt accessible, set it to blank.
		#~ if not os.path.exists(card["arturi"]):
			#~ card["arturi"] = "../../mono_bg.png"
		
		if "md5" not in card:
			hashstring = u""
			for m in HASHSORTORDER:
				hashstring += card[m]
			card["md5"] = hashlib.md5(hashstring.encode("utf-8")).hexdigest()
		card["md5sig1"] = card["md5"][:16]
		card["md5sig2"] = card["md5"][16:]
		
		#Handle newlines in the text
		card["text_block"] = ""
		numlines = 0
		for line in card["text"].split("\n"):
			if len(line) > 0:
				numlines += 1
				card["text_block"] += '<flowPara id="textBoxText'+str(numlines)+'">'+line+'</flowPara>'
		
		#Take a guess at what size works best.
		if "name_size" not in card:
			card["name_size"] = get_font_size(card["name"], 1)
		if "text_size" not in card:
			card["text_size"] = get_font_size_accurate(card["text"], debug=card["name"])
		if "typetext_size" not in card:
			card["typetext_size"] = get_font_size(card["type"], 1)
		
		if "rotate" not in card:
			if "rotation" in card:
				card["rotate"] = card["rotation"]
			else:
				card["rotate"] = 0
		
		#Get and set the width and height of the art.
		try:
			card["artwidth"], card["artheight"] = Image.open(card["arturi"]).size
		except (IOError):
			pass
		
		#Hide elements that would be "NUL" or an emdash.
		for a in ("name", "type", "text", "offense", "health", "cost", "spirit"):
			if a not in card:
				card[a+'vis'] = "none"
			elif card[a] == "NUL" or card[a] == "&#8212;" or card[a] == u'\u2014':
				card[a+'vis'] = "none"
			else:
				card[a+'vis'] = "inherit"
		
		#Now fill any missing replacements with an empty string
		done = False
		while not done:
			try:
				cardsvg.substitute(card) #Only tests for missing replacements
				done = True
			except (KeyError), e:
				card.update({str(e)[1:-1]:""}) #Strips the surrounding single quotes off of e
		return card

if __name__ == "__main__":
	parser = OptionParser()
	parser.add_option("-f", "--file", dest="filename", default=False,
		          help="generate svg for only FILE", metavar="FILE")
	parser.add_option("-i", "--stdin", dest="stdin", action="store_true", default=False,
		          help="generate svg from xml in stdin (not implemented)")
	parser.add_option("-o", "--stdout", dest="stdout", action="store_true", default=False,
		          help="output svg to stdout. Only works with -f or -i as well. ")
	parser.add_option("-m", "--monochrome", dest="MONOCHROME", action="store_true", default=False,
		          help="output svgs in monochrome and without images")
	parser.add_option("-d", "--cardsdir", dest="CARDSDIR", default="./cards/",
		          help="the directory to find files if -f isnt supplied, also used to determine where to save the svgs if not printing to stdout.")

	(options, args) = parser.parse_args()

	MONOCHROME = options.MONOCHROME
	CARDSDIR = options.CARDSDIR

	if not (options.filename or options.stdin):
		#for each folder in the cards directory:
		for folder in os.listdir(CARDSDIR):
			#for each xml file in the folder:
			for xmlfile in os.listdir(CARDSDIR+folder):
				#read in with minidom
				if xmlfile[-3:] == "xml":
					try:
						card = generate_card_dict(CARDSDIR+folder+"/"+xmlfile)
					except:
						raise "Could not parse: %s" % CARDSDIR+folder+"/"+xmlfile
			
					f = open(CARDSDIR+folder+"/"+xmlfile[:-3]+"svg", "w")
					data = cardsvg.substitute(card)
					
					f.write(data.encode("utf-8"))
					f.close()
	else:
		if options.stdin:
			#sys.stderr.write("reading from stdin")
			card = generate_card_dict(sys.stdin)
		else:
			card = generate_card_dict(options.filename)
		
		if options.stdout:
			print cardsvg.substitute(card)
		else:
			f = open(options.filename[-3:]+"svg", "w")
			data = cardsvg.substitute(card)
			f.write(data)
			f.close
